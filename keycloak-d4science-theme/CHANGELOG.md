This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "keycloak-d4science-themes"

## [v2.24.1]
Changes in other sub-components

## [v2.24.0]
New theme support for account (Keycloak.v3) and others (Keycloak.v2). `d4science.v2` theme has been deprecated (and removed from declarations) and now only `d4science` will be the theme to be used/extended.
Now the minor part of the version (the `24` in the `2.24.x`) shows the compatibility to the specific Keycloak major version, in this case `24.x.x`

## [v2.1.0]
- Revised terms, EU links, D4Science and Blue-Cloud logo updated (#25444)

## [v2.0.0]
- Updated d4science themes for Keycloak v19.x
- Reimplemented d4science.v2 account ui for Keycloak v16
- Porting from `d4science-keycloak-themes` repo
    - [#19518] Implemented the Keycloak "d4science" login parametric template.
    - Each new gateway that does not implement its own specific login template should use by default this base "d4science" template. However, it is possible to set some parameters in `<keycloak_home>/themes/<gatewayname>/login/themes.properties` file to do some customizations such as logo, background, colors, and some links (see for example parameters of dev4.d4science.org and next.d4science.org themes).
