# Keycloak D4Science Themes

**Keycloak D4Science Themes** module collects the implementations of base D4Science theme and a set of specific per gateway themes implementations.

Each Keycloak theme is made of a set of [Freemarker](https://freemarker.apache.org/) templates. Themes of Keycloak account section is implemented in [React](https://reactjs.org/).

## Structure of the project

The themes are in `src/main/resources/themes` subfolder.
The `util` subfolder contains some utility test scripts to create Keycloak clients that uses a specific theme.

## Built With

* [Node.js](https://nodejs.org/). See [Keycloak src project](https://github.com/keycloak/keycloak/tree/main/themes/src/main/resources/theme/keycloak.v2/account/src)
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

This is one of the modules that composes the EAR deployment defined in the "brother" module [keycloak-d4science-spi](../keycloak-d4science-spi-ear/README.md).

To build the module JAR file it is sufficient to type

    mvn clean package

### Installation

#### Qurkus based Keycloak

In order to deploy the module it is sufficient to copy into the `[keycloak-home]/providers` folder.

### Development

For details see [Theme section](https://www.keycloak.org/docs/latest/server_development/#_themes) of Keycloak developer docs.

As specified in [Deploying Themes](https://www.keycloak.org/docs/latest/server_development/#deploying-themes) section of the documentation, *themes can be deployed to Keycloak by copying the theme directory to themes or it can be deployed as an archive.*

In the development phase, we suggest to disable themes caching in Keycloak configuration file. To do this edit `standalone.xml`. For `theme` set `staticMaxAge` to `-1` and both `cacheTemplates` and `cacheThemes` to `false`.

To test theme in a Keycloak instance you can clone this repo somewhere and create a symbolic link for each theme you want to deploy, eg:

    mkdir /opt/git
    cd /opt/git
    git clone https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent.git

    cd /opt/keycloak/themes
    ln -s /opt/git/keycloak-d4science-spi-parent/keycloak-d4science-theme/src/main/resources/theme/d4science/
    ln -s /opt/git/keycloak-d4science-spi-parent/keycloak-d4science-theme/src/main/resources/theme/d4science.v2/

    ln -s /opt/git/keycloak-d4science-spi-parent/keycloak-d4science-theme/src/main/resources/theme/dev4.d4science.org/
    ...

Note: `d4science` and `d4science.v2` are the base D4Science themes, that are **required** by all `\*.d4science.org` per gateway specific themes.

If you are already logged-in in Keycloak administration console you have to logout and then re-login. At that you can set one of the new themes per Keycloak Realm or for a specific Keycloak Client in the realm.

## Change log

See [CHANGELOG.md](CHANGELOG.md) file for details

See [Releases](https://code-repo.d4science.org/gCubeSystem/d4science-keycloak-themes/releases).

## Authors

* **Vincenzo Cestone** ([Nubisware S.r.l.](http://www.nubisware.com))
* **Mauro Mugnaini** ([Nubisware S.r.l.](http://www.nubisware.com))

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260);
- the Seventh Framework Programme for research, technological development and demonstration
    - D4Science (grant no. 212488), D4Science-II (grant no.239019), ENVRI (grant no. 283465), EUBrazilOpenBio (grant no. 288754), iMarine(grant no. 283644);
- the H2020 research and innovation programme
    - BlueBRIDGE (grant no. 675680), EGIEngage (grant no. 654142), ENVRIplus (grant no. 654182), Parthenos (grant no. 654119), SoBigData (grant no. 654024),DESIRA (grant no. 818194), ARIADNEplus (grant no. 823914), RISIS2 (grant no. 824091), PerformFish (grant no. 727610), AGINFRAplus (grant no. 731001);
