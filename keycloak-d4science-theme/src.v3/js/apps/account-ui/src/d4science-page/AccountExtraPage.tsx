import {
  PageSection,
  PageSectionVariants,
} from "@patternfly/react-core";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { getSupportedLocales } from "../api/methods";
import { UserProfileMetadata } from "../api/representations";
import { Page } from "../components/page/Page";
import { useEnvironment } from "../root/KeycloakContext";
import { usePromise } from "../utils/usePromise";
import { AvatarForm } from './AvatarForm';

interface AccountExtraPageProps {
}

interface AccountExtraPageState {
    isModalOpen: boolean;
}

export const AccountExtraPage = () => {
  const { t } = useTranslation();
  const context = useEnvironment();
  const [userProfileMetadata, setUserProfileMetadata] = useState<UserProfileMetadata>();
  const [supportedLocales, setSupportedLocales] = useState<string[]>([]);
  const accountUrl = context.keycloak.createAccountUrl();

  usePromise(
    (signal) =>
      Promise.all([
        getSupportedLocales({ signal, context }),
      ]),
    ([supportedLocales]) => {
      setSupportedLocales(supportedLocales);
    },
  );

  return (
    <Page title={t('accountExtraInfoHtmlTitle')} description={t('accountExtraSubMessage')}>
      <PageSection isFilled variant={PageSectionVariants.light}>
        <AvatarForm accountUrl={accountUrl}></AvatarForm>
      </PageSection>
    </Page>
  );
};

export default AccountExtraPage;
