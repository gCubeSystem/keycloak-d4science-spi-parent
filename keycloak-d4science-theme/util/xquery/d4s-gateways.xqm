module namespace gw = 'urn:nubisware:d4science:gateways';

declare variable $gw:url := 'https://services.d4science.org/thematic-gateways';


declare function gw:list() {
  let $req := <http:request method="get" />
  return http:send-request($req, $gw:url)//a[@class='entry-link']/@href/data() ! replace(., '/explore', '')
};

