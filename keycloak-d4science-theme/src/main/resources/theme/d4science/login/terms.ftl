<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false displayWide=true; section>
    <#if section = "header">
        ${msg("termsTitle")}
    <#elseif section = "form">
    <div id="kc-terms-text" class="d4s-terms-text">
        <!-- D4Science terms and conditions ref -->
        <#include "terms.html" parse=false>
    </div>
    <div class="d4s-terms-info"><p>${msg("termsAcceptMsg")}</p></div>
    <form class="form-actions" action="${url.loginAction}" method="POST">
        <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="accept" id="kc-accept" type="submit" value="${msg("doAccept")}" disabled />
        <input class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" name="cancel" id="kc-decline" type="submit" value="${msg("doDecline")}"/>
    </form>
    <div class="clearfix"></div>

    <!-- D4Science enable "Accept" button -->
    <script type="text/javascript">
        document.getElementById("kc-terms-text").addEventListener("scroll", checkKcTermsScrollHeight, false);

        function checkKcTermsScrollHeight() {
            var kcTermsTextElement = document.getElementById("kc-terms-text")
            if ((kcTermsTextElement.scrollTop + kcTermsTextElement.offsetHeight + 10) >= kcTermsTextElement.scrollHeight){
                document.getElementById("kc-accept").disabled = false;
            }
        }
    </script>
    </#if>
</@layout.registrationLayout>
