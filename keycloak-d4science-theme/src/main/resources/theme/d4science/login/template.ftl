<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayRequiredFields=false displayWide=false>
<!DOCTYPE html>
<html class="${properties.kcHtmlClass!}"<#if realm.internationalizationEnabled> lang="${locale.currentLanguageTag}"</#if>>

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <#if properties.meta?has_content>
        <#list properties.meta?split(' ') as meta>
            <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
        </#list>
    </#if>
    <!-- D4Science custom page title and favicon -->
    <title><#if properties.titleTag?has_content>${properties.titleTag}<#else>${msg("loginTitle",(realm.displayName!''))}</#if></title>
    <link rel="icon" href="<#if properties.favicon?has_content>${properties.favicon}<#else>${url.resourcesPath}/img/favicon.ico</#if>" />
    <#if properties.stylesCommon?has_content>
        <#list properties.stylesCommon?split(' ') as style>
            <link href="${url.resourcesCommonPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.scripts?has_content>
        <#list properties.scripts?split(' ') as script>
            <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
        </#list>
    </#if>
    <script type="importmap">
        {
            "imports": {
                "rfc4648": "${url.resourcesCommonPath}/node_modules/rfc4648/lib/rfc4648.js"
            }
        }
    </script>
    <script src="${url.resourcesPath}/js/menu-button-links.js" type="module"></script>
    <#if scripts??>
        <#list scripts as script>
            <script src="${script}" type="text/javascript"></script>
        </#list>
    </#if>
    <#if authenticationSession??>
        <script type="module">
            import { checkCookiesAndSetTimer } from "${url.resourcesPath}/js/authChecker.js";

            checkCookiesAndSetTimer(
              "${authenticationSession.authSessionId}",
              "${authenticationSession.tabId}",
              "${url.ssoLoginInOtherTabsUrl?no_esc}"
            );
        </script>
    </#if>
</head>

<!-- D4Science body style overridden to handle bg image custom configs -->
<body class="${properties.kcBodyClass!}"
    style="<#if properties.contentBgImg?has_content>background: url('${url.resourcesPath}/${properties.contentBgImg!}') no-repeat center center fixed; background-size: cover; height: 100%;</#if> ${properties.contentStyle!}">
<div class="${properties.kcLoginClass!}">
    <!-- D4Science - does not want this header 
    <div id="kc-header" class="${properties.kcHeaderClass!}">
        <div id="kc-header-wrapper"
             class="${properties.kcHeaderWrapperClass!}">${kcSanitize(msg("loginTitleHtml",(realm.displayNameHtml!'')))?no_esc}</div>
    </div>
    -->
    <div style="${properties.headerReplacementStyle!}"></div>
    <div class="${properties.kcFormCardClass!} <#if displayWide>${properties.kcFormCardWideClass!}</#if>">

        <!-- D4Science logos header inside formCard -->
        <div style="${properties.logoHeaderStyle!}">
            <div>
                <#if client?? && client.getClientId()!='account'>
                    <a href="<#if client.getBaseUrl()?has_content>${client.getBaseUrl()}<#else>${"https://" + client.getClientId()}</#if>">
                        <#if properties.logoSrc?has_content>
                            <img class="img-fluid float-left" alt="${properties.logoAlt!}" src="${properties.logoSrc!}" style="${properties.logoStyle!}">
                        <#else>
                            <h1>
                                <#if client.getName()?has_content && client.getName()?starts_with("${")>${kcSanitize(msg(client.getName()?keep_after("{")?keep_before("}")))}
                                <#else>${(client.getName()!client.getClientId()!"undefined client")?capitalize?keep_before('.')}</#if>
                            </h1>
                        </#if>
                    </a>
                </#if>
            </div>
            <#if properties.infrastructureLogo?has_content && properties.infrastructureLogo='yes'>
                <div style="align-self: center;">
                    <a target="_blank" href="http://www.d4science.org">
                        <img class="img-fluid float-right" alt="D4Science Infrastructure" src="${url.resourcesPath}/img/PoweredByD4Science.png" style="${properties.infrastructureLogoStyle!}">
                    </a>
                </div>
            </#if>
        </div>

        <header class="${properties.kcFormHeaderClass!}">
            <#if realm.internationalizationEnabled  && locale.supported?size gt 1>
                <div class="${properties.kcLocaleMainClass!}" id="kc-locale">
                    <div id="kc-locale-wrapper" class="${properties.kcLocaleWrapperClass!}">
                        <div id="kc-locale-dropdown" class="menu-button-links ${properties.kcLocaleDropDownClass!}">
                            <button tabindex="1" id="kc-current-locale-link" aria-label="${msg("languages")}" aria-haspopup="true" aria-expanded="false" aria-controls="language-switch1">${locale.current}</button>
                            <ul role="menu" tabindex="-1" aria-labelledby="kc-current-locale-link" aria-activedescendant="" id="language-switch1" class="${properties.kcLocaleListClass!}">
                                <#assign i = 1>
                                <#list locale.supported as l>
                                    <li class="${properties.kcLocaleListItemClass!}" role="none">
                                        <a role="menuitem" id="language-${i}" class="${properties.kcLocaleItemClass!}" href="${l.url}">${l.label}</a>
                                    </li>
                                    <#assign i++>
                                </#list>
                            </ul>
                        </div>
                    </div>
                </div>
            </#if>
            <#if !(auth?has_content && auth.showUsername() && !auth.showResetCredentials())>
                <#if displayRequiredFields>
                    <div class="${properties.kcContentWrapperClass!}">
                        <div class="${properties.kcLabelWrapperClass!} subtitle">
                            <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
                        </div>
                        <div class="col-md-10">
                            <h1 id="kc-page-title"><#nested "header"></h1>
                        </div>
                    </div>
                <#else>
                    <h1 id="kc-page-title"><#nested "header"></h1>
                </#if>
            <#else>
                <#if displayRequiredFields>
                    <div class="${properties.kcContentWrapperClass!}">
                        <div class="${properties.kcLabelWrapperClass!} subtitle">
                            <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
                        </div>
                        <div class="col-md-10">
                            <#nested "show-username">
                            <div id="kc-username" class="${properties.kcFormGroupClass!}">
                                <label id="kc-attempted-username">${auth.attemptedUsername}</label>
                                <a id="reset-login" href="${url.loginRestartFlowUrl}" aria-label="${msg("restartLoginTooltip")}">
                                    <div class="kc-login-tooltip">
                                        <i class="${properties.kcResetFlowIcon!}"></i>
                                        <span class="kc-tooltip-text">${msg("restartLoginTooltip")}</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                <#else>
                    <#nested "show-username">
                    <div id="kc-username" class="${properties.kcFormGroupClass!}">
                        <label id="kc-attempted-username">${auth.attemptedUsername}</label>
                        <a id="reset-login" href="${url.loginRestartFlowUrl}" aria-label="${msg("restartLoginTooltip")}">
                            <div class="kc-login-tooltip">
                                <i class="${properties.kcResetFlowIcon!}"></i>
                                <span class="kc-tooltip-text">${msg("restartLoginTooltip")}</span>
                            </div>
                        </a>
                    </div>
                </#if>
            </#if>
        </header>
        <div id="kc-content">
            <div id="kc-content-wrapper">

                <#-- App-initiated actions should not see warning messages about the need to complete the action -->
                <#-- during login.                                                                               -->
                <#if displayMessage && message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
                    <div class="alert-${message.type} ${properties.kcAlertClass!} pf-m-<#if message.type = 'error'>danger<#else>${message.type}</#if>">
                        <div class="pf-c-alert__icon">
                            <#if message.type = 'success'><span class="${properties.kcFeedbackSuccessIcon!}"></span></#if>
                            <#if message.type = 'warning'><span class="${properties.kcFeedbackWarningIcon!}"></span></#if>
                            <#if message.type = 'error'><span class="${properties.kcFeedbackErrorIcon!}"></span></#if>
                            <#if message.type = 'info'><span class="${properties.kcFeedbackInfoIcon!}"></span></#if>
                        </div>
                        <span class="${properties.kcAlertTitleClass!}">${kcSanitize(message.summary)?no_esc}</span>
                    </div>
                </#if>

                <!-- D4Science: reintroduces displayWide=true for terms -->
                <#nested "form">

                <#if auth?has_content && auth.showTryAnotherWayLink()>
                    <form id="kc-select-try-another-way-form" action="${url.loginAction}" method="post">
                        <div class="${properties.kcFormGroupClass!}">
                            <input type="hidden" name="tryAnotherWay" value="on"/>
                            <a href="#" id="try-another-way"
                            onclick="document.forms['kc-select-try-another-way-form'].submit();return false;">${msg("doTryAnotherWay")}</a>
                        </div>
                    </form>
                </#if>

                <#nested "socialProviders">

                <#if displayInfo>
                    <div id="kc-info" class="${properties.kcSignUpClass!}">
                        <div id="kc-info-wrapper" class="${properties.kcInfoAreaWrapperClass!}">
                            <#nested "info">
                        </div>
                    </div>
                </#if>
            </div>
        </div>

        <!-- D4Science footer -->
        <footer style="${properties.footerStyle!}">
            <div>
                <a href="${properties.linkTerms!}">Terms of Use</a> | 
                <a href="${properties.linkCookies!'#'}">Cookies Policy</a> | 
                <a href="${properties.linkPrivacy!'#'}" target="_blank">Privacy Policy</a> | 
                <#if properties.linkProject?has_content><a href="${properties.linkProject!}" target="_blank">${properties.descrProject!}</a></#if>
            </div>
            <#if properties.ECLogo?has_content && properties.ECLogo='yes'>
                <div style="display: flex; padding-top: 10px; justify-content: space-between; gap: 10px;">
                    <#if properties.footerRow?has_content><div>${kcSanitize(properties.footerRow)?no_esc}</div></#if>
                    <div style="align-self: center;">
                        <a href="${properties.ECLogoLink!}" target="_blank">
                            <img class="float-right" alt="${properties.ECLogoAlt!}" src="${properties.ECLogoSrc!}" style="${properties.ECLogoStyle!}">
                        </a>
                    </div>
                </div>
            <#else>
                <#if properties.footerRow?has_content><div style="padding-top: 10px;">${kcSanitize(properties.footerRow)?no_esc}</div></#if>
            </#if>
        </footer>
        <script>
          // Replacing twitter icon that is not still available on this fa bundle
          let twitterUrl = '${url.resourcesPath}/img/twitter.svg';
          let ieleTwitter = document.querySelectorAll('.fa-twitter');
          ieleTwitter.forEach(e => {
            e.classList.remove('fa-twitter');
            e.innerHTML = '<img src="' + twitterUrl + '" style="width: 80%; height: 80%;" />';
          })
          // Replacing orcid icon that is not still available on this fa bundle
          let orchidUrl = '${url.resourcesPath}/img/orcid.svg';
          let ieleOrcid = document.querySelectorAll('.fa-orcid');
          ieleOrcid.forEach(e => {
            e.classList.remove('fa-orcid');
            e.innerHTML = '<img src="' + orchidUrl + '" style="width: 80%; height: 80%;" />';
          })
        </script>
    </div>
</div>
</body>
</html>
</#macro>
