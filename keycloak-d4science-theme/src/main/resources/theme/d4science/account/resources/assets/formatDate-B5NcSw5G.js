import{o as n}from"./index-CmO9OReA.js";const e={dateStyle:"long"},a={timeStyle:"short"},r={...e,...a};function T(t,o=r){return t.toLocaleString(n.languages,o)}export{T as f};
//# sourceMappingURL=formatDate-B5NcSw5G.js.map
