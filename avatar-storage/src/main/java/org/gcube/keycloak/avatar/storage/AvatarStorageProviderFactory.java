package org.gcube.keycloak.avatar.storage;

import org.keycloak.provider.ProviderFactory;

public interface AvatarStorageProviderFactory extends ProviderFactory<AvatarStorageProvider> {

}
