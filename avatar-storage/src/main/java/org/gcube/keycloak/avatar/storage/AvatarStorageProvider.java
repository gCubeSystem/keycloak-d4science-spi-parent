package org.gcube.keycloak.avatar.storage;

import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.provider.Provider;

import java.io.InputStream;

public interface AvatarStorageProvider extends Provider {

    void saveAvatarImage(RealmModel realm, UserModel user, InputStream input);

    InputStream loadAvatarImage(RealmModel realm, UserModel user);

    void deleteAvatarImage(RealmModel realm, UserModel user);

}
