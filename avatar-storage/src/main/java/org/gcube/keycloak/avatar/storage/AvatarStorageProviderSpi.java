package org.gcube.keycloak.avatar.storage;

import org.jboss.logging.Logger;
import org.keycloak.provider.Provider;
import org.keycloak.provider.ProviderFactory;
import org.keycloak.provider.Spi;

public class AvatarStorageProviderSpi implements Spi {

    private static final Logger logger = Logger.getLogger(AvatarStorageProviderSpi.class);

    public AvatarStorageProviderSpi() {
        logger.debug("Creating new avatar storage provider SPI");
    }

    @Override
    public boolean isInternal() {
        return false;
    }

    @Override
    public String getName() {
        return "avatar-storage";
    }

    @Override
    public Class<? extends Provider> getProviderClass() {
        return AvatarStorageProvider.class;
    }

    @Override
    public Class<? extends ProviderFactory<AvatarStorageProvider>> getProviderFactoryClass() {
        return AvatarStorageProviderFactory.class;
    }

}
