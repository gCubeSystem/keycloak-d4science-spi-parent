#!/bin/bash
$KEYCLOAK_HOME/bin/jboss-cli.sh --connect --controller=${1-localhost:9990} --command="/subsystem=keycloak-server:list-add(name=providers, value=module:org.gcube.keycloak.avatar-storage)"