# Avatar Storage

**Avatar Storage** defines a new [Keycloak](https://www.keycloak.org)'s SPI to plug avatar persistence strategy via `services` definition.

## Structure of the project

The source code is present in `src` folder.

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

This is one of the modules that composes the EAR deployment defined in the "brother" module [keycloak-d4science-spi](../keycloak-d4science-spi-ear/README.md).

To build the module JAR file it is sufficient to type

    mvn clean package

### Installation

#### Qurkus based Keycloak

In order to deploy the module it is sufficient to copy into the `[keycloak-home]/providers` folder.

#### Wildfly based Keycloak [DEPRECATED]

The module can be installed inside the locally running Wildfly based Keycloak runtime (when the Keycloak server is stopped) by using the shell file:

    install-keycloak-module.sh

Then, after the module has been installed and the server has been started, you can enable it with:

    add-avatar-resource-provider.sh [host:port]

This will make the new defined SPI available in the Wildfly based Keycloak server. The `[host:port]` parameter is optional and defaults to Wildfly control default host and port (`localhost:9990`)

(NOTE: Both commands are using the `$KEYCLOAK_HOME` environment variable to find where Keycloak server is located)

## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Mauro Mugnaini** ([Nubisware S.r.l.](http://www.nubisware.com))

## How to Cite this Software
[Intentionally left blank]

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

## Acknowledgments
[Intentionally left blank]