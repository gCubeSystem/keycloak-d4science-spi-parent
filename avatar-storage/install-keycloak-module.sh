#!/bin/bash
RESOURCES=(target/avatar-storage-*.jar)
$KEYCLOAK_HOME/bin/jboss-cli.sh --command="module add --name=org.gcube.keycloak.avatar-storage --resources=$RESOURCES --dependencies=org.keycloak.keycloak-core,org.keycloak.keycloak-services,org.keycloak.keycloak-server-spi,org.keycloak.keycloak-server-spi-private,org.jboss.logging"