# Keycloak D4Science SPI Parent

**Keycloak D4Science SPI Parent** is the master POM of some modules that extend the [Keycloak](https://www.keycloak.org)'s functionalities via its provided SPIs and some new SPIs. 

## Structure of the project

The project is a Maven master POM project and it is composed of several modules:

* [avatar-importer](avatar-importer/README.md)
* [avatar-realm-resource](avatar-realm-resource/README.md)
* [avatar-storage](avatar-storage/README.md)
* [event-listener-provider](event-listener-provider/README.md)
* [identity-provider-mapper](identity-provider-mapper/README.md)
* [keycloak-d4science-theme](keycloak-d4science-theme/README.md)
* [keycloak-d4science-script](keycloak-d4science-script/README.md)
* [ldap-storage-mapper](ldap-storage-mapper/README.md)
* [protocol-mapper](protocol-mapper/README.md)

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

To build the project (with all the modules) it is sufficient to type

    mvn clean package

## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Mauro Mugnaini** ([Nubisware S.r.l.](http://www.nubisware.com))
* **Marco Lettere** ([Nubisware S.r.l.](http://www.nubisware.com))
* **Vincenzo Cestone** ([Nubisware S.r.l.](http://www.nubisware.com))

## How to Cite this Software
[Intentionally left blank]

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

## Acknowledgments
[Intentionally left blank]