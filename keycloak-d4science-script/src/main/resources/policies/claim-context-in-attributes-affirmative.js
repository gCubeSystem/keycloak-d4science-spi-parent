var context = $evaluation.getContext();
var permission = $evaluation.getPermission();
var identity = context.getIdentity();
var attributes = identity.getAttributes();

var resource = permission.getResource();

print("============= " + resource.getName() + "[" + resource.getOwner() + "," + resource.getResourceServer()  + "] =================")

var extra_claims = permission.claims;
print("Extra claims: " + extra_claims)
if (extra_claims !== null) {
  var ctx = extra_claims.context;
  if (ctx !== null) {
    var context = ctx.toArray()[0]
    if (context !== null) {
      print("context="+context)
      var resource_attribute = resource.getAttribute(context);
      if (resource_attribute !== null) {
        print("resource attribute=" + resource_attribute)
	$evaluation.grant();
      } else $evaluation.deny();
    } else $evaluation.deny();
  } else $evaluation.deny();
} else $evaluation.deny();

