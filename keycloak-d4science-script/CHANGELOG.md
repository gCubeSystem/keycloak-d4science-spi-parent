This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "keycloak-d4science-themes"

## [v2.24.1]
Changes in other sub-components

## [v2.24.0]
Now the minor part of the version (the `24` in the `2.24.x`) shows the compatibility to the specific Keycloak major version, in this case `24.x.x`

## [v2.1.0]
Changes in other sub-components

## [v2.0.0]
- Porting from first custom implementation not in SCM/versioned