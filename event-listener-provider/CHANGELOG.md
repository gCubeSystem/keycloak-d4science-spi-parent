This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "event-listener-provider"

## [v2.24.1]
Changes in other sub-components

## [v2.24.0]
Completely rewrote the implementation and configuration method, now both `admin` and `d4science` realms are configured by default as interesting realms.
Added specific SPI configurations with `spi-events-listener-orchestrator-event-publisher-` prefix: `include-realms`, `exclude-realms`, `include-admin-types`, `exclude-admin-types`, `include-events` and `exclude-events`
Now the minor part of the version (the `24` in the `2.24.x`) shows the compatibility to the specific Keycloak major version, in this case `24.x.x`

## [v2.1.0]
Changes in other sub-components

## [v2.0.0]
- Updated to be compiled/used with Keycloak v19.x

## [v1.1.0]
Now calls to orchestrators are UMA authenticated with `keycloak-client` credentials and `conductor-server` audience

## [v1.0.1]
Only interesting events are published

## [v1.0.0]
Prepared for the release in pre-prod

## [v0.2.0]
- Extracted sub-module from the original project. Send Keycloak's events to an orchestrator endpoint
