# Event Listener Provider

**Event Listener Provider** extends the [Keycloak](https://www.keycloak.org)'s event SPI to push JSON events to an orchestrator endpoint.

## Structure of the project

The source code is present in `src` folder.

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

This is one of the modules that composes the EAR deployment defined in the "brother" module [keycloak-d4science-spi](../keycloak-d4science-spi-ear/README.md).

To build the JAR file it is sufficient to type

    mvn clean package

### Installation

#### Qurkus based Keycloak

In order to deploy the module it is sufficient to copy into the `[keycloak-home]/providers` folder.

### Configuration

In order to configure the `Orchestrator`'s server endpoint URL it is sufficient to create a new client inside the `d4science` realm (or in the realm specified in the `target-orchestrator-realm` configuration) with id `conductor-server` and set-up the API URL in the `Home URL` field (e.g. https://conductor.dev.d4science.org/api/workflow/).

To perform authorized requests to the `orchestrator`, with an UMA token as `bearer`, a specific client with id `keycloak-server` has to be created in the same realm of the orchestrator client. This client should be configured to obtain an UMA ticket with audience: `conductor-server`.
Additionally, if the `Home URL` for this client is configured with a token endpoint URL, this will be used to obtain the UMA ticket, instead of the token URL of the Keycloak instance where the SPI is running.

Since version `2.24.0` it is possible to configure the SPI configurations with the following entries:

* `target-orchestrator-realm` - to specify in which realm looking for the the configured clients, overriding the default that is: `d4science`
* `include-realms` - to specify the interesting realms (default are: `d4science` and `master`)
* `exclude-realms` - to specify the non interesting realms
* `include-admin-types` - To specify the admin's `ResourceType`s to include (default are: `user`, `client` and `realm`)
* `exclude-admin-types`- To specify the admin's `ResourceType`s to exclude
* `include-events` - To specify the `EventType`s to include (default are: `register` and `delete_account`)
* `exclude-events` - To specify the `EventType`s to exclude

The prefix to add for the configuration is  `spi-events-listener-orchestrator-event-publisher-` and f.e. to configure the interesting realms it is sufficient to use:

```bash
    spi-events-listener-orchestrator-event-publisher-include-realms=d4science,master
```

This can be achieved either by adding some lines to the `[keycloak-home]/conf/keycloak.conf` configuration file, by using the CLI parameters or using the ENV variables.
(Please, refer to the [Keycloak documentation section](https://www.keycloak.org/server/configuration) for further info) 

## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Marco Lettere** ([Nubisware S.r.l.](http://www.nubisware.com))
* **Mauro Mugnaini** ([Nubisware S.r.l.](http://www.nubisware.com))

## How to Cite this Software
[Intentionally left blank]

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

## Acknowledgments
[Intentionally left blank]