This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "avatar-importer"

## [v2.24.1]
Changes in other sub-components

## [v2.24.0]
Removed LinkedIn OAuth2 deprecated provider and added the new OIDC version to supported providers
Moved from `commons-lang` to `commons-lang3` artifactId in `org.apache.commons` groupId.
Now the minor part of the version (the `24` in the `2.24.x`) shows the compatibility to the specific Keycloak major version, in this case `24.x.x`

## [v2.1.0]
Changes in other sub-components

## [v2.0.0]
- Updated to be compiled/used with Keycloak v19.x

## [v1.1.0]
Changes in other sub-components

## [v1.0.1]
Changes in other sub-components

## [v1.0.0]
Prepared for the release in pre-prod

## [v0.2.0]
- First release. It provides avatar import from Identity Providers login. (#19726).<br>
  Currently supported IdPs:
    - Google
    - Facebook
    - LinkedIn
