# Avatar Importer

**Avatar Importer** extends the [Keycloak](https://www.keycloak.org)'s Identity Provider (IdP) mapper SPI to import the avatar image from the IdP used for the login by default; if no image is provided by the IdP in the proper claim the importer can be set-up (enabled by default) to try to load the avatar using the `libravatar` (and `gravatar`) service.

## Structure of the project

The source code is present in `src` folder.
* Relevant information about how the repository is organized.
* Description of the Maven modules (if any). 
* Any information needed to work with the code.

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

This is one of the modules that composes the (DEPRECATED) EAR deployment defined in the "brother" module [keycloak-d4science-bundle](../keycloak-d4science-bundle/README.md).

To build the module's JAR file it is sufficient to type

    mvn clean package

### Installation

#### Quarkus based Keycloak

In order to deploy the module it is sufficient to copy into the `[keycloak-home]/providers` folder.

### Configuration

Once deployed, the mapper must be enabled for the realm in the specific Identity Provider Mappers.

Specific settings are present:

- **`Use Libravatar service`**: Enable or not the avatar discovery on the libravatar (gravatar) service
	
- **`Import avatar in a separate thread`**: the import of the avatar will be performed asynch in a separate thread to improve speed during a new identity import

## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Marco Lettere** ([Nubisware S.r.l.](http://www.nubisware.com))
* **Mauro Mugnaini** ([Nubisware S.r.l.](http://www.nubisware.com))

## How to Cite this Software
[Intentionally left blank]

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

## Acknowledgments
[Intentionally left blank]