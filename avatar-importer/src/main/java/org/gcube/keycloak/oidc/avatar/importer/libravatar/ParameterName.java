package org.gcube.keycloak.oidc.avatar.importer.libravatar;

public enum ParameterName {

    DEFAULT("default", "d"),

    SIZE("size", "s"),

    FORCE_DEFAULT("forcedefault", "f");

    private final String name;
    private final String shortName;

    private ParameterName(String name, String shortName) {
        this.name = name;
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

}