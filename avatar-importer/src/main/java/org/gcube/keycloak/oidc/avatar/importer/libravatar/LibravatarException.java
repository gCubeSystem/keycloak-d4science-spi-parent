package org.gcube.keycloak.oidc.avatar.importer.libravatar;

public class LibravatarException extends Exception {

	/**
	 * Serial code version <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 2574665849051070802L;

	/**
	 * Default constructor
	 */
	public LibravatarException() {
		super();
	}

	/**
	 * 
	 * @param message Exception message text
	 */
	public LibravatarException(String message) {
		super(message);
	}

	public LibravatarException(Throwable root) {
		super(root);
	}

	public LibravatarException(String message, Throwable root) {
		super(message, root);
	}
}