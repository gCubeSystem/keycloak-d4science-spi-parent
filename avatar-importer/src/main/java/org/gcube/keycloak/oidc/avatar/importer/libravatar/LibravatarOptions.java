package org.gcube.keycloak.oidc.avatar.importer.libravatar;

import java.util.HashMap;
import java.util.Map;

import com.google.common.base.Preconditions;

public class LibravatarOptions {

    /**
     * Specifies a custom base URI for HTTP use. The default is to use the
     * official libravatar HTTP server. If you *really* wanted to use a non-free
     * server, you could set this to "http://gravatar.com/avatar/", but why
     * would you do such a thing?
     */
    private static String baseUri = "http://cdn.libravatar.org/avatar/";

    /**
     * Specifies a custom base URI for HTTPS use. The default is to use the
     * official libravatar HTTPS server.
     */
    private static String secureBaseUri = "https://seccdn.libravatar.org/avatar/";

    /**
     * Produce https:// URIs where possible. This avoids mixed-content warnings
     * in browsers when using libravatar-sharp from within a page served via
     * HTTPS.
     **/
    private boolean useHttps;

    /**
     * Use the SHA256 hash algorithm, rather than MD5. SHA256 is significantly
     * stronger, but is not supported by Gravatar, so libravatar's fallback to
     * Gravatar for missing images will not work. Note that using
     * AvatarUri.FromOpenID implicitly uses SHA256.
     */
    private boolean useSHA256;

    /**
     * URI for a default image, if no image is found for the user. This also
     * accepts any of the "special" values in AvatarDefaultImages
     */
    private LibravatarDefaultImage defaultImage;

    /**
     * Size of the image requested. Valid values are between 1 and 512 pixels.
     * The default size is 80 pixels.
     */
    private Integer imageSize;

    /**
     * It is sometimes interesting to test the default option, even for hashes with matching avatars.
     * This behavior can be triggered by providing the extra parameter <code>forcedefault</code>
     * or <code>f</code> to the URL with a value of <code>y</code>:
     */
    private boolean forceDefault;

    public LibravatarOptions withHttps() {
        this.useHttps = true;
        return this;
    }

    public LibravatarOptions useSHA256() {
        this.useSHA256 = true;
        return this;
    }

    public LibravatarOptions withImageSize(final Integer imageSize) {
        Preconditions.checkArgument(imageSize >= 1 && imageSize <= 512);
        this.imageSize = imageSize;
        return this;
    }

    public LibravatarOptions withoutHttps() {
        this.useHttps = false;
        return this;
    }

    public LibravatarOptions defaultingTo(LibravatarDefaultImage defaultImage) {
        this.defaultImage = defaultImage;
        return this;
    }

    public boolean isUseHttps() {
        return useHttps;
    }

    public boolean isUseSHA256() {
        return useSHA256;
    }

    public Boolean hasDefaultImageSet() {
        return defaultImage != null;
    }

    public LibravatarDefaultImage getDefaultImage() {
        return defaultImage;
    }

    public Boolean hasImageSizeSet() {
        return imageSize != null;
    }

    public Integer getImageSize() {
        return imageSize;
    }

    public boolean isForceDefault() {
        return forceDefault;
    }

    public Map<String, String> toParametersMap() {
        return toParametersMap(false);
    }

    public Map<String, String> toParametersMap(boolean useLongNames) {
        Map<String, String> parametersMap = new HashMap<>();
        if (hasImageSizeSet()) {
            ParameterName p = ParameterName.SIZE;
            parametersMap.put(useLongNames ? p.getName() : p.getShortName(), String.valueOf(getImageSize()));
        }

        if (hasDefaultImageSet()) {
            ParameterName p = ParameterName.DEFAULT;
            parametersMap.put(useLongNames ? p.getName() : p.getShortName(), getDefaultImage().getCode());
        }

        if (isForceDefault()) {
            ParameterName p = ParameterName.FORCE_DEFAULT;
            parametersMap.put(useLongNames ? p.getName() : p.getShortName(), "y");
        }
        return parametersMap;
    }

    public static void setBaseUri(String baseUri) {
        LibravatarOptions.baseUri= baseUri;
    }

    public static String getBaseUri() {
        return baseUri;
    }

    public static void setSecureBaseUri(String secureBaseUri) {
        LibravatarOptions.secureBaseUri= secureBaseUri;
    }

    public static String getSecureBaseUri() {
        return secureBaseUri;
    }

}