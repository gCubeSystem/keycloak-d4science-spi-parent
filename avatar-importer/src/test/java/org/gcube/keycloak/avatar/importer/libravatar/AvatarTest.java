package org.gcube.keycloak.avatar.importer.libravatar;

import static org.junit.Assert.assertNotNull;

import java.io.InputStream;

import org.gcube.keycloak.oidc.avatar.importer.libravatar.Libravatar;
import org.gcube.keycloak.oidc.avatar.importer.libravatar.LibravatarDefaultImage;
import org.gcube.keycloak.oidc.avatar.importer.libravatar.LibravatarException;
import org.gcube.keycloak.oidc.avatar.importer.libravatar.LibravatarOptions;
import org.junit.AssumptionViolatedException;
import org.junit.Test;

public class AvatarTest {

    @Test
    public void testOK() {
        try {
            InputStream is = Libravatar.from("mauro.mugnaini@nubisware.com").withOptions(new LibravatarOptions().withHttps()).download();
            assertNotNull(is);
        } catch (LibravatarException e) {
            // Remote system is not working properly
            throw new AssumptionViolatedException("Cannot connect with external service", e);
        }
    }

    @Test(expected = LibravatarException.class)  
    public void testException() throws LibravatarException {
        Libravatar.from("fake@superdomainboya.de").withOptions(new LibravatarOptions().withHttps().defaultingTo(LibravatarDefaultImage.NOT_FOUND)).downloadArray();
    }

}
