package org.gcube.keycloak.avatar.importer.libravatar;

import static org.junit.Assert.assertNull;

import org.gcube.keycloak.oidc.avatar.importer.libravatar.DNSUtil;
import org.junit.Test;

public class DNSUtilTest {

    @Test
    public void testKO() {
        String url = DNSUtil.resolveSecureSRVRecordFromEmail("mauro@fakedomainwithlognnametobesearchedfor.com");
        assertNull(url);
    }

// To be enabled when a domain that expose avatars is found    
//    public void testOK() {    
//        System.out.println(DNSUtil.resolveSecureSRVRecordFromEmail("fake@libravatar.org"));
//    }

}
