This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "identity-provider-mapper"

## [v2.24.1]
Added `D4ScienceDynamicScopeContextMapper` to use new `dynamic scopes` preview feature and a `scope` parameter in token request (e.g. `d4s-context:%2Fgcube%2FdevNext%2FNextNext`)

## [v2.24.0]
Now the minor part of the version (the `24` in the `2.24.x`) shows the compatibility to the specific Keycloak major version, in this case `24.x.x`

## [v2.1.0]
- Provided the `D4ScienceContextMapper` that maps the D4S context requested in a customizable HTTP header as token's claim having the configured name (defaulting to the 'aud' claim). Can also shrink the `resource access` token claim to have only the requested context entry.
