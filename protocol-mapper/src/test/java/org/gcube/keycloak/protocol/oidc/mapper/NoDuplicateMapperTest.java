package org.gcube.keycloak.protocol.oidc.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collection;
import java.util.ServiceLoader;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.junit.Test;
import org.keycloak.protocol.ProtocolMapper;

/**
 * Original code repo: https://github.com/mschwartau/keycloak-custom-protocol-mapper-example
 */
public class NoDuplicateMapperTest {

    @Test
    public void shouldNotHaveMappersWithDuplicateIds() {
        final ServiceLoader<ProtocolMapper> serviceLoader = ServiceLoader.load(ProtocolMapper.class);
        final Collection<String> mapperIds = StreamSupport.stream(serviceLoader.spliterator(), false).map(elem -> elem.getId()).collect(Collectors.toList());

        assertThat(mapperIds).doesNotHaveDuplicates();
    }
}
