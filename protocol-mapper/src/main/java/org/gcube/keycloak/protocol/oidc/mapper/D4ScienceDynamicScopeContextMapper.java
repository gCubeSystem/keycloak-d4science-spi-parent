package org.gcube.keycloak.protocol.oidc.mapper;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.jboss.logging.Logger;
import org.keycloak.models.ClientSessionContext;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.ProtocolMapperModel;
import org.keycloak.models.UserSessionModel;
import org.keycloak.protocol.oidc.mappers.AbstractOIDCProtocolMapper;
import org.keycloak.protocol.oidc.mappers.OIDCAccessTokenMapper;
import org.keycloak.protocol.oidc.mappers.OIDCAttributeMapperHelper;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.rar.AuthorizationDetails;
import org.keycloak.representations.AccessToken;
import org.keycloak.representations.AccessToken.Access;
import org.keycloak.representations.IDToken;

public class D4ScienceDynamicScopeContextMapper extends AbstractOIDCProtocolMapper implements OIDCAccessTokenMapper {

    private static final Logger logger = Logger.getLogger(D4ScienceDynamicScopeContextMapper.class);

    public static final String DYNAMIC_SCOPE_NAME = "d4scm.dynamic-scope-name";
    public static final String OVERWRITE_AUD = "d4scm.overwrite-aud";
    public static final String NARROW_RESOURCE_ACCESS = "d4scm.narrow-ra";

    private static final Map<String, ProviderConfigProperty> CONFIG_PROPERTIES_MAP = new HashMap<>();

    // Assuring that the mapper is executed as last
    private static final int PRIORITY = Integer.MAX_VALUE;
    private static final String DISPLAY_TYPE = "OIDC D4Science Dynamic Scope Context Mapper";
    private static final String PROVIDER_ID = "oidc-d4science-dynamic-scope-context-mapper";

    public static final String DEFAULT_DYNAMIC_SCOPE_NAME = "d4s-context";
    public static final String DEFAULT_TOKEN_CLAIM = "d4s_context";
    public static final String AUD_TOKEN_CLAIM = "aud";
    public static final ProtocolMapperModel audProtocolMapperModel = OIDCAttributeMapperHelper.createClaimMapper(null,
            null, AUD_TOKEN_CLAIM, ProviderConfigProperty.STRING_TYPE, true, true, true, null);

    static {
        List<ProviderConfigProperty> providerConfigProperties = new ArrayList<>();
        OIDCAttributeMapperHelper.addTokenClaimNameConfig(providerConfigProperties);
        providerConfigProperties.forEach(conf -> {
            if (OIDCAttributeMapperHelper.TOKEN_CLAIM_NAME.equals(conf.getName()))
                conf.setDefaultValue(DEFAULT_TOKEN_CLAIM);

            conf.setReadOnly(true);
        });
        OIDCAttributeMapperHelper.addIncludeInTokensConfig(providerConfigProperties,
                D4ScienceDynamicScopeContextMapper.class);

        ProviderConfigProperty dynamicScopeProperty = new ProviderConfigProperty();
        dynamicScopeProperty.setName(DYNAMIC_SCOPE_NAME);
        dynamicScopeProperty.setLabel("Dynamic scope name with the requested context");
        dynamicScopeProperty.setHelpText(
                "The HTTP header that contains the requested context to be mapped in the configured claim");
        dynamicScopeProperty.setType(ProviderConfigProperty.STRING_TYPE);
        dynamicScopeProperty.setDefaultValue(DEFAULT_DYNAMIC_SCOPE_NAME);
        //        dynamicScopeProperty.setType(ProviderConfigProperty.LIST_TYPE);
        //        dynamicScopeProperty.setOptions();
        dynamicScopeProperty.setReadOnly(true);
        dynamicScopeProperty.setRequired(true);
        providerConfigProperties.add(dynamicScopeProperty);

        ProviderConfigProperty overwriteProperty = new ProviderConfigProperty();
        overwriteProperty.setName(OVERWRITE_AUD);
        overwriteProperty.setLabel("Add/overwrite `aud` claim?");
        overwriteProperty.setType(ProviderConfigProperty.BOOLEAN_TYPE);
        overwriteProperty.setHelpText("Overwrite the `aud` claim to the requested context entry");
        overwriteProperty.setDefaultValue("true");
        providerConfigProperties.add(overwriteProperty);

        ProviderConfigProperty narrowProperty = new ProviderConfigProperty();
        narrowProperty.setName(NARROW_RESOURCE_ACCESS);
        narrowProperty.setLabel("Narrow down resource access array?");
        narrowProperty.setType(ProviderConfigProperty.BOOLEAN_TYPE);
        narrowProperty.setHelpText("Narrow down resource access claim to contain only the requested context entry");
        narrowProperty.setDefaultValue("true");
        providerConfigProperties.add(narrowProperty);

        // Creating the configurations map
        providerConfigProperties.forEach(conf -> CONFIG_PROPERTIES_MAP.put(conf.getName(), conf));
    }

    @Override
    public String getDisplayCategory() {
        return TOKEN_MAPPER_CATEGORY;
    }

    @Override
    public int getPriority() {
        return PRIORITY;
    }

    @Override
    public String getDisplayType() {
        return DISPLAY_TYPE;
    }

    @Override
    public String getHelpText() {
        return "Maps the D4Science context audience by reading the configured header's value and sets it as the configured token claim, if it is in scope";
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        //      CONFIG_PROPERTIES_MAP.get(DYNAMIC_SCOPE_NAME).setOptions(TBF);
        return new ArrayList<>(CONFIG_PROPERTIES_MAP.values());
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    protected void setClaim(final IDToken token,
            final ProtocolMapperModel mappingModel,
            final UserSessionModel userSession,
            final KeycloakSession keycloakSession,
            final ClientSessionContext clientSessionCtx) {

        // Since only the OIDCAccessTokenMapper interface is implemented, we are almost sure that
        // the token object is an AccessToken but adding a specific check anyway
        if (token instanceof AccessToken) {
            AccessToken accessToken = (AccessToken) token;
            final String dynamicScopeName = mappingModel.getConfig().get(DYNAMIC_SCOPE_NAME);

            Optional<AuthorizationDetails> authorizationDetailsOptional = clientSessionCtx
                    .getAuthorizationRequestContext().getAuthorizationDetailEntries().stream()
                    .filter(details -> details.isDynamicScope()
                            && details.getClientScope().getName().equals(dynamicScopeName))
                    .findFirst();

            if (authorizationDetailsOptional.isPresent()) {
                String requestedD4SContext = authorizationDetailsOptional.get().getDynamicScopeParam();
                // Checking if the context has been passed as urlencoded or not and encoding it if is necessary
                if (requestedD4SContext.startsWith("/")) {
                    try {
                        logger.debugf("Requested context as decoded string, urlencoding it: %s", clientSessionCtx);
                        requestedD4SContext = URLEncoder.encode(requestedD4SContext, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        logger.error("Cannot encode context: " + requestedD4SContext, e);
                    }
                }

                if (requestedD4SContext != null && !"".equals(requestedD4SContext)) {
                    logger.debugf("Checking resource access for the requested context: %s", requestedD4SContext);
                    Access contextAccessInResourceAccess = accessToken.getResourceAccess(requestedD4SContext);
                    if (accessToken.getResourceAccess().isEmpty() || contextAccessInResourceAccess != null) {
                        logger.infof("Mapping context %s as the configured claim: %s", requestedD4SContext,
                                mappingModel.getConfig().get(OIDCAttributeMapperHelper.TOKEN_CLAIM_NAME));

                        OIDCAttributeMapperHelper.mapClaim(token, mappingModel, requestedD4SContext);

                        if (Boolean.parseBoolean(mappingModel.getConfig().get(OVERWRITE_AUD))) {
                            logger.infof("Adding/overwriting `aud` claim with %s", requestedD4SContext);

                            OIDCAttributeMapperHelper.mapClaim(token, audProtocolMapperModel, requestedD4SContext);
                        }

                        if (Boolean.parseBoolean(mappingModel.getConfig().get(NARROW_RESOURCE_ACCESS))) {
                            logger.infof("Removing all access details but the requested context: %s",
                                    requestedD4SContext);
                            accessToken.getResourceAccess().clear();
                            accessToken.getResourceAccess().put(requestedD4SContext, contextAccessInResourceAccess);
                        }
                    } else {
                        logger.warnf("Requested context '%s' is not accessible to the client: %s", requestedD4SContext,
                                clientSessionCtx.getClientSession().getClient().getName());
                    }
                } else {
                    logger.tracef("Authorization detail for '%s' dynamic scope not found in request", dynamicScopeName);
                }
            }
        }
    }

}