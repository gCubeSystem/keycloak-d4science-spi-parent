# Identity Provider Mapper

**Protocol Mapper** extends the [Keycloak](https://www.keycloak.org)'s OIDC protocol mappers SPI. In this first release is introduced the possibility to specify the token audience by reading the value of a custom `X-D4Science-Context` HTTP header.

## Structure of the project

The source code is present in `src` folder.

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

This is one of the modules that composes the EAR deployment defined in the "brother" module [keycloak-d4science-spi](../keycloak-d4science-spi-ear/README.md).

To build the JAR artifact it is sufficient to type

    mvn clean package

### Installation

#### Qurkus based Keycloak

In order to deploy the module it is sufficient to copy into the `[keycloak-home]/providers` folder.

## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Marco Lettere** ([Nubisware S.r.l.](http://www.nubisware.com))
* **Mauro Mugnaini** ([Nubisware S.r.l.](http://www.nubisware.com))

## How to Cite this Software
[Intentionally left blank]

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

## Acknowledgments
[Intentionally left blank]