/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.gcube.keycloak.storage.ldap.mappers;

import java.util.List;

import org.keycloak.component.ComponentModel;
import org.keycloak.component.ComponentValidationException;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.provider.ProviderConfigurationBuilder;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.UserStorageProviderModel;
import org.keycloak.storage.ldap.LDAPConfig;
import org.keycloak.storage.ldap.LDAPStorageProvider;
import org.keycloak.storage.ldap.mappers.AbstractLDAPStorageMapper;
import org.keycloak.storage.ldap.mappers.AbstractLDAPStorageMapperFactory;
import org.keycloak.storage.ldap.mappers.LDAPConfigDecorator;

/**
 * @author <a href="mailto:mposolda@redhat.com">Marek Posolda</a>
 * @author <a href="mailto:mauro.mugnaini@nubisware.com">Mauro Mugnaini</a>
 */
public class UserAttributeTemplatedLDAPStorageMapperFactory extends AbstractLDAPStorageMapperFactory
        implements LDAPConfigDecorator {

    public static final String PROVIDER_ID = "ua-templated-ldap-mapper";

    protected static final List<ProviderConfigProperty> configProperties;

    static {
        List<ProviderConfigProperty> props = getConfigProps(null);
        configProperties = props;
    }

    static List<ProviderConfigProperty> getConfigProps(ComponentModel p) {
        String readOnly = "false";
        UserStorageProviderModel parent = new UserStorageProviderModel();
        if (p != null) {
            parent = new UserStorageProviderModel(p);
            LDAPConfig ldapConfig = new LDAPConfig(parent.getConfig());
            readOnly = ldapConfig.getEditMode() == UserStorageProvider.EditMode.WRITABLE ? "false" : "true";
        }
        ProviderConfigurationBuilder config = ProviderConfigurationBuilder.create()
                .property().name(UserAttributeTemplatedLDAPStorageMapper.TEMPLATE_ATTRIBUTE)
                .label("Template string")
                .helpText("Template to be used to compute final value to be set in LDAP. You can user the "
                        + UserAttributeTemplatedLDAPStorageMapper.ATTRIBUTE_VALUE
                        + " placeholder in the text and it will be replaced by the value read by the model property. "
                        + "(e.g. /home/" + UserAttributeTemplatedLDAPStorageMapper.ATTRIBUTE_VALUE)
                .type(ProviderConfigProperty.STRING_TYPE)
                .add()
                .property().name(UserAttributeTemplatedLDAPStorageMapper.USER_MODEL_ATTRIBUTE)
                .label("User Model Attribute")
                .helpText(
                        "Name of the UserModel property or attribute you want to map the LDAP attribute into. For example 'firstName', 'lastName, 'email', 'street' etc.")
                .type(ProviderConfigProperty.STRING_TYPE)
                .add()
                .property().name(UserAttributeTemplatedLDAPStorageMapper.LDAP_ATTRIBUTE).label("LDAP Attribute")
                .helpText("Name of mapped attribute on LDAP object. For example 'cn', 'sn, 'mail', 'street' etc.")
                .type(ProviderConfigProperty.STRING_TYPE)
                .add()
                .property().name(UserAttributeTemplatedLDAPStorageMapper.READ_ONLY).label("Read Only")
                .helpText(
                        "Read-only attribute is imported from LDAP to UserModel, but it's not saved back to LDAP when user is updated in Keycloak.")
                .type(ProviderConfigProperty.BOOLEAN_TYPE)
                .defaultValue(readOnly)
                .add();
        if (parent.isImportEnabled()) {
            config.property().name(UserAttributeTemplatedLDAPStorageMapper.ALWAYS_READ_VALUE_FROM_LDAP)
                    .label("Always Read Value From LDAP")
                    .helpText(
                            "If on, then during reading of the LDAP attribute value will always used instead of the value from Keycloak DB")
                    .type(ProviderConfigProperty.BOOLEAN_TYPE).defaultValue("false").add();
        }
        config.property().name(UserAttributeTemplatedLDAPStorageMapper.IS_MANDATORY_IN_LDAP)
                .label("Is Mandatory In LDAP")
                .helpText(
                        "If true, attribute is mandatory in LDAP. Hence if there is no value in Keycloak DB, the empty value will be set to be propagated to LDAP")
                .type(ProviderConfigProperty.BOOLEAN_TYPE)
                .defaultValue("false").add();

        return config.build();
    }

    @Override
    public String getHelpText() {
        return "Used to map single attribute from LDAP user to attribute of UserModel in Keycloak DB";
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return configProperties;
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    public void validateConfiguration(KeycloakSession session, RealmModel realm, ComponentModel config)
            throws ComponentValidationException {

        checkMandatoryConfigAttribute(UserAttributeTemplatedLDAPStorageMapper.USER_MODEL_ATTRIBUTE,
                "User Model Attribute", config);

        checkMandatoryConfigAttribute(UserAttributeTemplatedLDAPStorageMapper.LDAP_ATTRIBUTE, "LDAP Attribute",
                config);
    }

    @Override
    protected AbstractLDAPStorageMapper createMapper(ComponentModel mapperModel,
            LDAPStorageProvider federationProvider) {
        return new UserAttributeTemplatedLDAPStorageMapper(mapperModel, federationProvider);
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties(RealmModel realm, ComponentModel parent) {
        return getConfigProps(parent);
    }

    @Override
    public void updateLDAPConfig(LDAPConfig ldapConfig, ComponentModel mapperModel) {
    }
}
