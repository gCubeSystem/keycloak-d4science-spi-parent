This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "keycloak-d4science-spi-parent"

## [v2.24.1]
### `protocol-mapper` module
- Added `D4ScienceDynamicScopeContextMapper` to use new `dynamic scopes` preview feature and a `scope` parameter in token request (e.g. `d4s-context:%2Fgcube%2FdevNext%2FNextNext`)

## [v2.24.0]
Now the minor part of the version (the `24` in the `2.24.x`) shows the compatibility to the specific Keycloak major version, in this case `24.x.x`

## [v2.1.0]
- Added new [protocol-mapper](protocol-mapper/README.md) module to make the custom protocol mappers available
- Revised terms, EU links, D4Science and Blue-Cloud logo updated (#25444)

## [v2.0.0]
- Updated to be compiled/used with Keycloak v19.0.2
- Added themes sub-module to be included in the EAR artifact, porting from `d4science-keycloak-themes` repo

## [v1.1.0]
Updated athentication with bearer code took from new examples: added realm param. Now calls to orchestrators are UMA authenticated with `keycloak-client` credentials and `conductor-server` audience

## [v1.0.1]
user put in disable status and avatar deleted at user delete request, params returned correctly adding avatar

## [v1.0.0]
Added delete-account sub-module and prepared for the release in pre-prod

## [v0.2.0]
Completed the refactoring as Maven multi-module project and each project has been documented as the new project template advise. It also provide the avatar support (#19726)

## [v0.1.0]
- Refactored as Maven multi-module project and support for avatar (#19726)

## [v0.0.1]
- First release (#19657, #19684)
