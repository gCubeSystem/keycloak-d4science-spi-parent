This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "identity-provider-mapper"

## [v2.24.1]
Changes in other sub-components

## [v2.24.0]
Now the minor part of the version (the `24` in the `2.24.x`) shows the compatibility to the specific Keycloak major version, in this case `24.x.x`

## [v2.1.0]
Changes in other sub-components

## [v2.0.0]
- Updated to be compiled/used with Keycloak v19.x

## [v1.1.0]
changes in other sub-components

## [v1.0.1]
changes in other sub-components

## [v1.0.0]
Prepared for the release in pre-prod

## [v0.2.0]
- Extracted sub-module from the original project. Extends the identity provider mapper SPI to extract the username for the users that use and IdP to subscribe itself to the d4science portal.
