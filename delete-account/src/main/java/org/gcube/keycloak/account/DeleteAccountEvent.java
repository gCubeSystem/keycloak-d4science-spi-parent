package org.gcube.keycloak.account;

import org.gcube.event.publisher.Event;
import org.gcube.keycloak.event.KeycloakEvent;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

public class DeleteAccountEvent extends Event {

    private static final long serialVersionUID = -4519708230339210727L;

    public static final String NAME = "delete-user-account";

    public static final String REALM = "realm";
    public static final String USER_ID = "userid";

    public DeleteAccountEvent(UserModel userModel, RealmModel realmModel) {
        super(NAME, KeycloakEvent.TYPE, KeycloakEvent.HOST_NAME);
        setUserId(userModel.getId());
        setRealm(realmModel.getName());
    }

    public void setUserId(String userid) {
        set(USER_ID, userid);
    }

    public String getUserId() {
        return (String) get(USER_ID);
    }
    public void setRealm(String realm) {
        set(KeycloakEvent.REALM, realm);
    }

    public String getRealm() {
        return (String) get(KeycloakEvent.REALM);
    }

}
