package org.gcube.keycloak.account;

import org.jboss.logging.Logger;
import org.keycloak.models.KeycloakSession;
import org.keycloak.services.resource.RealmResourceProvider;

public class DeleteAccountRealmResourceProvider implements RealmResourceProvider {

    protected static final Logger logger = Logger.getLogger(DeleteAccountRealmResourceProvider.class);

    private KeycloakSession session;

    public DeleteAccountRealmResourceProvider(KeycloakSession session) {
        logger.info("Created new DeleteAccountRealmResourceProvider object");
        this.session = session;
    }

    @Override
    public Object getResource() {
        return new DeleteAccountResource(session);
    }

    @Override
    public void close() {
    }

}
