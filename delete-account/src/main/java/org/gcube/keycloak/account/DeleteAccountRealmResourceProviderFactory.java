package org.gcube.keycloak.account;

import org.jboss.logging.Logger;
import org.keycloak.Config.Scope;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.services.resource.RealmResourceProviderFactory;

public class DeleteAccountRealmResourceProviderFactory implements RealmResourceProviderFactory {

    protected static final Logger logger = Logger.getLogger(DeleteAccountRealmResourceProviderFactory.class);

    public static final String ID = "account-delete";

    public DeleteAccountRealmResourceProviderFactory() {
        logger.info("Created new DeleteAccountRealmResourceProviderFactory object");
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public DeleteAccountRealmResourceProvider create(KeycloakSession session) {
        return new DeleteAccountRealmResourceProvider(session);
    }

    @Override
    public void init(Scope config) {
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
    }

    @Override
    public void close() {
    }

}
