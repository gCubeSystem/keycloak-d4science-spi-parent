This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "delete-account"

## [v2.24.0]
This module has been deprecated and disabled/removed from build since now the built in delete account functionality can be used.

## [v2.1.0]
Changes in other sub-components

## [v2.0.0]
- Updated to be compiled/used with Keycloak v19.x
- New URI path mappings for resource

## [v1.1.0]
Now calls to orchestrators are UMA authenticated with `keycloak-client` credentials and `conductor-server` audience

## [v1.0.1]
User put in disable status and avatar deleted at user delete request

## [v1.0.0]
Prepared for the release in pre-prod

## [v0.2.0-SNAPSHOT]
- First released version. Provides "delete account" handling function, called from Keycloak's account management page button, by sending a delete account event to the orchestrator that will perform the activity.

