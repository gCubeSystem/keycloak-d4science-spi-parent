# Avatar Realm Resource

**Avatar Realm Resource** defines the new avatar resource inside Keycloak and exposes it on REST, implements the SPI defined in the `avatar-storage` module to store avatars on a S3 persistence, on file system or in an user's property; the use of the last option is discouraged since the DB's property mapping could be limited by the JDBC driver/JPA settings/DB defaults.

## Structure of the project

The source code is present in `src` folder.

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

This is one of the modules that composes the (DEPRECATED) EAR deployment defined in the "brother" module [keycloak-d4science-spi](../keycloak-d4science-spi-ear/README.md).

To build the JAR artifact it is sufficient to type

    mvn clean package

### Installation

The type of the storage to be used is configured in the `org.gcube.keycloak.avatar.storage.AvatarStorageProviderFactory` file under the `META-INF/services/` folder of the module source/JAR.

This uses the S3 persistence by default and the content of the file is: `org.gcube.keycloak.avatar.storage.s3.MinioAvatarStorageProviderFactory`

#### Quarkus based Keycloak

In order to deploy the module it is sufficient to copy into the `[keycloak-home]/providers` folder.

### Configuration

In order to configure the storage provider you have can either add some lines to the `[keycloak-home]/conf/keycloak.conf` configuration file, use the CLI parameters or use the ENV variables.
(Please, refer to the [Keycloak documentation section](https://www.keycloak.org/server/configuration) for further info)

Let's see the specific setting for every provider.

#### Minio avatar storage provider

To configure the S3 endpoint, name and credentials you should use the following configuration lines:

    spi-avatar-storage-avatar-storage-s3-server-url=[server-address]
    spi-avatar-storage-avatar-storage-s3-access-key=[access-key]
    spi-avatar-storage-avatar-storage-s3-secret-key=[secret-key]
    spi-avatar-storage-avatar-storage-s3-root-bucket=[bucket-name]

#### File avatar storage provider

If you need to customize the folder where avatars images are saved (defaulting `[user-home]/avatar`) you can add the following line to the `[keycloak-home]/conf/keycloak.conf` file:

    spi-avatar-storage-avatar-storage-file-avatar-folder=[full-path-to-the-avatar-folder]

(For example: `spi-avatar-storage-avatar-storage-file-avatar-folder=/opt/keycloak/avatar` )

By default the `username` of the user is used as a filename for the image into the filesystem but is possible to use the user's email simply adding the following line in the previously seen configuration file:

    spi-avatar-storage-avatar-storage-file-use-email-as-filename=true

## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Mauro Mugnaini** ([Nubisware S.r.l.](http://www.nubisware.com))

## How to Cite this Software
[Intentionally left blank]

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

## Acknowledgments
[Intentionally left blank]