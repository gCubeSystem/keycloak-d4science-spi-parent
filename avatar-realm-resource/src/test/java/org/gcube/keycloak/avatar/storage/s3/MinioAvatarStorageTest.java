package org.gcube.keycloak.avatar.storage.s3;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.io.FileNotFoundException;

import org.gcube.keycloak.avatar.storage.s3.MinioAvatarStorageProvider.Configuration;
import org.jboss.logging.Logger;
import org.junit.Test;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.mockito.Mockito;

public class MinioAvatarStorageTest {

    private static final Logger logger = Logger.getLogger(MinioAvatarStorageTest.class);

    private static final String ENDPOINT_URL = "https://isti-cloud.isti.cnr.it:13808";
    private static final String ACCESS_KEY_ENV = "ACCESS_KEY";
    private static final String SECRET_KEY_ENV = "SECRET_KEY";
    private static final String BUCKET = "keycloak-data-dev";

    private static final boolean ALSO_DELETE_OBJECT = true;

    @Test
    public void test() throws FileNotFoundException {
        String accessKey = System.getenv(ACCESS_KEY_ENV);
        String secretKey = System.getenv(SECRET_KEY_ENV);
        if (accessKey == null || secretKey == null) {
            logger.error("Cannot proceed with tests without access and secret keys");
            return;
        }
        Configuration minioConfig = new Configuration(ENDPOINT_URL, accessKey, secretKey, BUCKET);

        MinioAvatarStorageProvider minioAvatarStorageProvider = new MinioAvatarStorageProvider(minioConfig);
        RealmModel realmModel = Mockito.mock(RealmModel.class);
        when(realmModel.getName()).thenReturn("d4science");
        UserModel userModel = Mockito.mock(UserModel.class);
        when(userModel.getUsername()).thenReturn("test.user");

        minioAvatarStorageProvider.saveAvatarImage(realmModel, userModel,
                this.getClass().getClassLoader().getResourceAsStream("tm-avatar.png"));

        assertNotNull(minioAvatarStorageProvider.loadAvatarImage(realmModel, userModel));
        if (ALSO_DELETE_OBJECT) {
            minioAvatarStorageProvider.deleteAvatarImage(realmModel, userModel);
            assertNull(minioAvatarStorageProvider.loadAvatarImage(realmModel, userModel));
            // Delete of a non existing resource must not raise an exception, assuring it with a proper test
            minioAvatarStorageProvider.deleteAvatarImage(realmModel, userModel);
        }
    }

}
