package org.gcube.keycloak.avatar.storage.file;

import org.gcube.keycloak.avatar.storage.AvatarStorageProvider;
import org.gcube.keycloak.avatar.storage.AvatarStorageProviderFactory;
import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

public class FileAvatarStorageProviderFactory implements AvatarStorageProviderFactory {

    public static final String DEFAULT_AVATAR_FOLDER = (
            System.getProperty("jboss.server.data.dir") != null
                ? System.getProperty("jboss.server.data.dir")
                : System.getProperty("user.home")
            ) + "/avatar";

    public static final Boolean USE_EMAIL_AS_FILENAME_DEFAULT = Boolean.FALSE;

    private String avatarFolder;
    private Boolean useEmailAsFilename;

    @Override
    public AvatarStorageProvider create(KeycloakSession session) {
        return new FileAvatarStorageProvider(avatarFolder, useEmailAsFilename);
    }

    @Override
    public void init(Config.Scope config) {
        avatarFolder = config.get("avatar-folder", DEFAULT_AVATAR_FOLDER);
        useEmailAsFilename = config.getBoolean("use-email-as-filename", USE_EMAIL_AS_FILENAME_DEFAULT);
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
        // NOOP
    }

    @Override
    public void close() {
        // NOOP
    }

    @Override
    public String getId() {
        return "avatar-storage-file";
    }

}