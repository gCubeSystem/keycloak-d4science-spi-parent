package org.gcube.keycloak.avatar.storage.s3;

import org.gcube.keycloak.avatar.storage.AvatarStorageProvider;
import org.gcube.keycloak.avatar.storage.AvatarStorageProviderFactory;
import org.gcube.keycloak.avatar.storage.s3.MinioAvatarStorageProvider.Configuration;
import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

public class MinioAvatarStorageProviderFactory implements AvatarStorageProviderFactory {

    private Configuration minioConfig;

    @Override
    public AvatarStorageProvider create(KeycloakSession session) {
        return new MinioAvatarStorageProvider(minioConfig);
    }

    @Override
    public void init(Config.Scope config) {

        String serverUrl = config.get("server-url");
        String accessKey = config.get("access-key");
        String secretKey = config.get("secret-key");
        String rootBucket = config.get("root-bucket");

        this.minioConfig = new Configuration(serverUrl, accessKey, secretKey, rootBucket);
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
        // NOOP
    }

    @Override
    public void close() {
        // NOOP
    }

    @Override
    public String getId() {
        return "avatar-storage-s3";
    }

}