package org.gcube.keycloak.avatar;

import java.nio.file.Files;

import org.gcube.keycloak.avatar.storage.AvatarStorageProvider;
import org.jboss.resteasy.annotations.cache.NoCache;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import org.keycloak.common.ClientConnection;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.ForbiddenException;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotAuthorizedException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class AvatarAdminResource extends AbstractAvatarResource {

    @Context
    private AvatarStorageProvider avatarStorageProvider;

    @Context
    private HttpHeaders httpHeaders;

    @Context
    private ClientConnection clientConnection;

    public AvatarAdminResource(KeycloakSession session) {
        super(session);
//        tokenManager = new TokenManager();
    }

    public void init() {
        checkRealmAdmin();
    }

    @GET
    @Path("/{user_id}")
    @Produces({ "image/png", "image/jpeg", "image/gif" })
    public Response downloadUserAvatarImage(@PathParam("user_id") String userId) {
        try {
            UserModel user = session.users().getUserById(session.getContext().getRealm(), userId);
            return fetchAndCreateResponse(session.getContext().getRealm(), user);
        } catch (ForbiddenException e) {
            return Response.status(Response.Status.FORBIDDEN).entity(e.getMessage()).build();
        } catch (Exception e) {
            logger.error("error getting user avatar", e);
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @POST
    @NoCache
    @Path("/{user_id}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadUserAvatarImage(@PathParam("user_id") String userId,  @RestForm(AVATAR_IMAGE_PARAMETER) FileUpload image) {
        try {
            if (auth == null) {
                return Response.status(Response.Status.UNAUTHORIZED).build();
            }

            RealmModel realm = session.getContext().getRealm();
            UserModel user = session.users().getUserById(session.getContext().getRealm(), userId);

            saveUserImage(realm, user, Files.newInputStream(image.uploadedFile()));
        } catch (ForbiddenException e) {
            return Response.status(Response.Status.FORBIDDEN).entity(e.getMessage()).build();
        } catch (Exception e) {
            logger.error("error saving user avatar", e);
            return Response.serverError().entity(e.getMessage()).build();
        }

        return Response.ok().build();
    }

    private void checkRealmAdmin() {
        if (auth == null) {
            throw new NotAuthorizedException("Bearer");
        } else if (auth.getToken().getRealmAccess() == null
                || !auth.getToken().getRealmAccess().isUserInRole("admin")) {

            throw new ForbiddenException("Does not have realm admin role");
        }
    }
}
