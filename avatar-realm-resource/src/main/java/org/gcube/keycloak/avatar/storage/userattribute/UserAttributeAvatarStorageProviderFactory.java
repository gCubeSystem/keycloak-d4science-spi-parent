package org.gcube.keycloak.avatar.storage.userattribute;

import org.gcube.keycloak.avatar.storage.AvatarStorageProvider;
import org.gcube.keycloak.avatar.storage.AvatarStorageProviderFactory;
import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

public class UserAttributeAvatarStorageProviderFactory implements AvatarStorageProviderFactory {

    public static final String USER_ATTRIBUTE_NAME = "avatar";

    @Override
    public AvatarStorageProvider create(KeycloakSession session) {
        return new UserAttributeAvatarStorageProvider(USER_ATTRIBUTE_NAME);
    }

    @Override
    public void init(Config.Scope config) {
        // NOOP
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
        // NOOP
    }

    @Override
    public void close() {
        // NOOP
    }

    @Override
    public String getId() {
        return "avatar-storage-user_attribute";
    }

}