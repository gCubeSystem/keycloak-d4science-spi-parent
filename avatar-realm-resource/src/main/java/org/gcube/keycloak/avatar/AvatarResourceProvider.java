package org.gcube.keycloak.avatar;

import org.jboss.logging.Logger;
import org.keycloak.models.KeycloakSession;
import org.keycloak.services.resource.RealmResourceProvider;

public class AvatarResourceProvider implements RealmResourceProvider {

    private static final Logger logger = Logger.getLogger(AvatarResourceProvider.class);

    private final KeycloakSession keycloakSession;

    public AvatarResourceProvider(KeycloakSession keycloakSession) {
        logger.debugf("New avatar resource provider created for session: %s",
                keycloakSession.getContext().getContextPath());

        this.keycloakSession = keycloakSession;
    }

    @Override
    public Object getResource() {
        logger.trace("Getting new avatar resource");
        return new AvatarResource(keycloakSession);
    }

    @Override
    public void close() {
        logger.trace("Closing avatar resource provider");
    }
}
