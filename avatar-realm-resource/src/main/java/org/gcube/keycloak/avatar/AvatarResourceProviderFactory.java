package org.gcube.keycloak.avatar;

import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.services.resource.RealmResourceProvider;
import org.keycloak.services.resource.RealmResourceProviderFactory;

import static org.keycloak.Config.Scope;

import org.jboss.logging.Logger;

public class AvatarResourceProviderFactory implements RealmResourceProviderFactory {

    private static final Logger logger = Logger.getLogger(AvatarResourceProviderFactory.class);

    public static final String ID = "account-avatar";

    public AvatarResourceProviderFactory() {
        logger.debug("Creating new avatar resource provider factory");
    }

    @Override
    public RealmResourceProvider create(KeycloakSession keycloakSession) {
        return new AvatarResourceProvider(keycloakSession);
    }

    @Override
    public void init(Scope scope) {
    }

    @Override
    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {
    }

    @Override
    public void close() {
        // NOOP
    }

    @Override
    public String getId() {
        return ID;
    }
}
