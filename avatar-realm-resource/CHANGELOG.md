This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "avatar-realm-resource"

## [v2.24.1]
Changes in other sub-components

## [v2.24.0]
Moved code from `Resteasy classic` to `Reateasy reactive` since now Quarkus uses this framework.
Now the minor part of the version (the `24` in the `2.24.x`) shows the compatibility to the specific Keycloak major version, in this case `24.x.x`

## [v2.1.0]
Changes in other sub-components

## [v2.0.0]
- Updated to be compiled/used with Keycloak v19.x
- New URI path mappings for resource

## [v1.1.0]
Updated athentication with bearer code took from new examples: added realm param

## [v1.0.1]
Params returned correctly adding avatar

## [v1.0.0]
Prepared for the release in pre-prod

## [v0.2.0]
- First released version. Provides avatar resource, exposes via REST interface, can be configured in the UI with proper interface. It also provide two implementation for the storage: one file system based and one user's attribute based (to be tested with an external the DB implementation, it works well with H2 default DB).

